
FROM  docker-test.revdebug.com/python-revdebug:3.9
WORKDIR /app

COPY ./ .

RUN pip3 install django==2.2.0
RUN pip3 install psycopg2-binary
RUN pip3 install wrapt
RUN pip3 install celery==4.4.7
RUN pip3 install redis
RUN pip3 install django-celery-beat
RUN pip3 install django-celery-results



