import random
from celery import shared_task

@shared_task(name="sum_two_numbers")
def add(x, y):
    return x + y

@shared_task(name="multiply_two_numbers")
def mul(x, y):
    total = x * (y * random.randint(3, 100))
    return total

@shared_task(name="sum_list_numbers")
def xsum(numbers):
    return sum(numbers)

@shared_task(name="kaboom")
def boom(numbers):
    a=0
    for x in range(0,100):
       print('')
    raise "KABOOM"
    return sum(numbers)

from celery.decorators import task
from celery.utils.log import get_task_logger
from time import sleep

logger = get_task_logger(__name__)

@task(name='my_first_task')
def my_first_task(duration):
    print("do")
    sleep(duration)
    print("done")
    # raise "KABOOM"
    return('first_task_done')


import requests
@task(name='req_task')
def req_task(duration):
    # r = requests.get('http://40.89.147.34:7777/req', auth=('user', 'pass'))
    return('req')