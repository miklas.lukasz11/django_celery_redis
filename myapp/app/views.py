from django.shortcuts import render
from django.http import HttpResponse
import psycopg2
# from celery.decorators import task
from app.tasks import *
from .tasks import my_first_task, req_task
import requests

def index(request):
    return HttpResponse("hello there")

def celery(request):
    my_first_task.delay(10)
    add(1,2)
    return HttpResponse("hello there")

def err(request):
    my_first_task.delay(10)
    boom(1)
    return HttpResponse("hello there")


def req(request):
    req_task.delay(10)
    return HttpResponse("hello there")

# Create your views here.
