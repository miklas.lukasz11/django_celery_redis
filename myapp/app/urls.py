from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add', views.celery, name='celery'),
    path('err', views.err, name='err'),
    path('req', views.req, name='req'),
]