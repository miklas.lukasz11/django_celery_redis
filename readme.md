Redis
sudo docker run --name=redis-devel --publish=6379:6379 --hostname=redis --restart=on-failure --detach redis:latest


Set redis url in settings.py

>BROKER_URL = 'redis://localhost:6379'\
CELERY_RESULT_BACKEND = 'redis://localhost:6379'\

In manage.py directory
>celery -A myapp worker -l info
>python manage.py runserver


